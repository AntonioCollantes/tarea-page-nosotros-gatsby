exports.createPages = async ({ actions, graphql, reporter }) => {
  const resultado = await graphql(`
    query {
      allDatoCmsRegion {
        nodes {
          slug
        }
      }
    }
  `)
  if (resultado.errors) {
    reporter.panic("No hubo resultados", resultado.errors)
  }

  const regiones = resultado.data.allDatoCmsRegion.nodes

  regiones.forEach(region => {
    actions.createPage({
      path: region.slug,
      component: require.resolve("./src/templates/regiones.jsx"),
      context: {
        slug: region.slug,
      },
      defer: true,
    })
  })

  // const { createPage } = actions
  // createPage({
  //   path: "/using-dsg",
  //   component: require.resolve("./src/templates/using-dsg.js"),
  //   context: {},
  //   defer: true,
  // })
}
