import { useStaticQuery, graphql } from "gatsby"

const colores = [
  `linear-gradient(to right bottom, #ffb900cc, #ff773080)`,
  `linear-gradient(to right bottom, #2998ff, #5643fa7d)`,
  `linear-gradient(to right bottom, rgba(123, 213, 11, 0.8), rgba(40, 180, 133, 0.8))`,
]

const useRegiones = () => {
  const { allDatoCmsRegion } = useStaticQuery(graphql`
    query {
      allDatoCmsRegion {
        nodes {
          titulo
          contenido
          slug
          imagen {
            fluid {
              ...GatsbyDatoCmsFluid
            }
          }
        }
      }
    }
  `)

  const info = allDatoCmsRegion.nodes.map((region, index) => {
    return {
      ...region,
      color: colores[index],
    }
  })
  return [info]
}

export default useRegiones
