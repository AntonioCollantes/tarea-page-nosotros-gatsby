import React from "react"
import Layout from "../components/layout"
import ContenidoNosotros from "../components/ContenidoNosotros"
import ImagenSlideNosotros from "../components/ImagenSlide/nosotros"

const Nosotros = () => {
  return (
    <Layout>
      <ImagenSlideNosotros />
      <ContenidoNosotros />
    </Layout>
  )
}

export default Nosotros
