import * as React from "react"

import Layout from "../components/layout"
import Seo from "../components/seo"

import useRegiones from "../hooks/useRegiones"

import ImagenSlide from "../components/ImagenSlide"
import ContenidoInicio from "../components/ContenidoInicio"
import ContenidoCards from "../components/ContenidoCards"

const IndexPage = () => {
  const [info] = useRegiones()

  console.log("mira la info")
  console.log(info)

  return (
    <Layout>
      <Seo title="The Dark Knight (trilogía)" />
      <ImagenSlide />
      <ContenidoInicio />
      <ContenidoCards regiones={info} />
    </Layout>
  )
}

export default IndexPage
