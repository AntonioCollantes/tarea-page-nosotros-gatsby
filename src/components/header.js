import * as React from "react"
import { Link } from "gatsby"

const Header = () => (
  <nav className="navbar navbar-expand-lg navbar-light bg-primary">
    <Link className="navbar-brand" to="/">
      Logo
    </Link>

    <div className="navbar-collapse">
      <ul className="navbar-nav mr-auto">
        <li className="nav-item active">
          <Link className="nav-link text-white" activeClassName="active" to="/">
            Inicio
          </Link>
        </li>
        <li className="nav-item">
          <Link
            className="nav-link text-white"
            activeClassName="active"
            to="nosotros"
          >
            Nosotros
          </Link>
        </li>
      </ul>
    </div>
  </nav>
)

export default Header
