import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"
// import { GatsbyImage, getImage } from "gatsby-plugin-image"

const ContenidoInicio = () => {
  const { allDatoCmsPagina } = useStaticQuery(graphql`
    query {
      allDatoCmsPagina(filter: { slug: { eq: "inicio" } }) {
        nodes {
          titulo
          contenido
          imagen {
            fluid {
              ...GatsbyDatoCmsFluid
            }
          }
        }
      }
    }
  `)

  const { titulo, contenido, imagen } = allDatoCmsPagina.nodes[0]

  return (
    <div className="container">
      <h2 className="text-center text-success mt-5">{titulo}</h2>
      <div className="row mt-5">
        <div className="col-md-5">{contenido}</div>
        <div className="col-md-5">
          <Img fluid={imagen.fluid} />
        </div>
      </div>
    </div>
  )
}

export default ContenidoInicio
