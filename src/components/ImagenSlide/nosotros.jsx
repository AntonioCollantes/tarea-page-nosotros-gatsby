import React from "react"
import BackgroundImage from "gatsby-background-image"
import { useStaticQuery, graphql } from "gatsby"
import { slide } from "./index.module.css"

const ImagenSlideNosotros = () => {
  const { image } = useStaticQuery(graphql`
    query {
      image: file(relativePath: { eq: "nat-10.jpg" }) {
        sharep: childImageSharp {
          fluid {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  const backgroundFluidImageStack = [
    image.sharep.fluid,
    `linear-gradient(to right bottom, rgba(123, 213, 11, 0.8), rgba(40, 180, 133, 0.8))`,
  ].reverse()

  return (
    <div>
      <BackgroundImage
        Tag="section"
        className={slide}
        fluid={backgroundFluidImageStack}
      >
        <section
          style={{ height: "100%" }}
          className="d-flex flex-column justify-content-center alight-item-center"
        >
          <h1 className="mb-4 text-white text-center">
            Bienvenidos a Batman
          </h1>
          <p className="text-white text-center" style={{ fontSize: "20px" }}>
            La trilogía de The Dark Knight
          </p>
        </section>
      </BackgroundImage>
    </div>
  )
}

export default ImagenSlideNosotros
