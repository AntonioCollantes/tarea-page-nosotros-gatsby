import React from "react"
import BackgroundImage from "gatsby-background-image"
import { image, title } from "./index.module.css"
import { Link } from "gatsby"

const Card = ({ region }) => {
  const { titulo, contenido, imagen, slug, color } = region

  const backgroundFluidImageStack = [imagen.fluid, color].reverse()

  return (
    <div className="card mb-4" style={{ width: "20rem" }}>
      <BackgroundImage className={image} fluid={backgroundFluidImageStack}>
        <section
          style={{ height: "100%" }}
          className="d-flex flex-column justify-content-center alight-items-center"
        >
          <h5 className={title}>{titulo}</h5>
        </section>
      </BackgroundImage>
      <div className="card-body">
        <p className="card-text">{contenido}</p>
        <Link className="btn btn-primary" to={`/${slug}`}>
          VER INFORMACION
        </Link>
      </div>
    </div>
  )
}

export default Card
