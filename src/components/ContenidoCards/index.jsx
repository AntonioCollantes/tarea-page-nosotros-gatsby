import React from "react"
import Card from "../Card"

const ContenidoCards = ({ regiones }) => {
  return (
    <div className="mt-5 container">
      <h2 className="text-center text-success">La trilogía de The Dark Knight</h2>
      <div className="d-flex justify-content-between flex-wrap mb-5">
        {regiones.map(region => (
          <Card key={region.slug} region={region} />
        ))}
      </div>
    </div>
  )
}

export default ContenidoCards
