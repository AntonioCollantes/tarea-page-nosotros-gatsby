import React from "react"
import Img from "gatsby-image"
import Layout from "../components/layout"
import { graphql } from "gatsby"

export const query = graphql`
  query ($slug: String) {
    allDatoCmsRegion(filter: { slug: { eq: $slug } }) {
      nodes {
        titulo
        contenido
        slug
        imagen {
          fluid {
            ...GatsbyDatoCmsFluid
          }
        }
      }
    }
  }
`
const Regiones = ({
  data: {
    allDatoCmsRegion: { nodes },
  },
}) => {
  const { titulo, contenido, imagen } = nodes[0]
  return (
    <Layout>
      <div className="container">
        <h1 className="text-center text-success"> {titulo}</h1>
        <p>{contenido}</p>
        <Img fluid={imagen.fluid} />
      </div>
    </Layout>
  )
}

export default Regiones
